jQuery(function ($) {

	// every array in 'params' is one set of strings that - if present in the current url - activates multiSelect.
	// eg ['rexsearch', 'settings'] activates multiSelect for the rexsearch settings page.
	// also the outer wrapper of the multiselect will get the joined strings as class for styling (e.g. 'rexsearch-settings')
	var params = [
			['xform','manager','data_edit']
		],
		css_class = [],
		check_params = function(e) {
			var url = document.URL;
			css_class = [];
			return e.every(function (e) {
				if (url.indexOf(e) === -1) {
					return false;
				}
				css_class.push(e);
				return true;
			});
		};

	if (params.some(check_params))
	{
		$('#rex-wrapper').find('select[multiple]').each(function () {
			$(this).multiSelect({
				selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Auswählbare filtern'>",
				selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Ausgewählte filtern'>",
				afterInit: function(ms){
					var that = this,
						$selectableSearch = that.$selectableUl.prev(),
						$selectionSearch = that.$selectionUl.prev(),
						selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
						selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

					that.$container.addClass(css_class.join('-'));

					that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
						.on('keydown', function(e){
							if (e.which === 40){
								that.$selectableUl.focus();
								return false;
							}
						});

					that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
						.on('keydown', function(e){
							if (e.which == 40){
								that.$selectionUl.focus();
								return false;
							}
						});
				},
				afterSelect: function(){
					this.qs1.cache();
					this.qs2.cache();
				},
				afterDeselect: function(){
					this.qs1.cache();
					this.qs2.cache();
				}
			});
		});
	}
});