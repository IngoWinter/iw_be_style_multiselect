<?php
$mypage = 'iw_be_style_multiselect';
if($REX['REDAXO'])
{
    $header = '
    <!-- '.$mypage.' -->
    <link href="../files/addons/be_style/plugins/'.$mypage.'/vendor/lou/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
    <link href="../files/addons/be_style/plugins/'.$mypage.'/agk.css" media="screen" rel="stylesheet" type="text/css">
    <script src="../files/addons/be_style/plugins/'.$mypage.'/vendor/lou/js/jquery.multi-select.js"></script>
    <script src="../files/addons/be_style/plugins/'.$mypage.'/vendor/riklomas/jquery.quicksearch.js"></script>
    <script src="../files/addons/be_style/plugins/'.$mypage.'/init.js"></script>
    <!-- end '.$mypage.' -->
    ';
    $header_include = 'return $params["subject"].\''.$header.'\';';
    rex_register_extension('PAGE_HEADER', create_function('$params',$header_include));
}
?>
